# Ingelec Group App

### Descripción
Aplicación móvil para formularios de los servicios.

### Tecnlogías
- Ionic v1
- Generator-M-Ionic v1.12.0

### Requisitos
- Bower
- Gulp
- Android Kit
- ADB Devices

### Instalación
1. Clonar repositorio https://gitlab.com/alejandrocfc/ingelec-app
2. Dentro de la carpeta instalar dependencias ``npm install`` y ``bower install``
3. Comandos de ejecución
    - ``gulp --cordova 'prepare'`` Prepara y limpia carpeta para despliegue
    - ``gulp --cordova 'run android'`` Compila aplicación en dispositivo Android físico o emulador
    - ``gulp --livereload 'run android'`` Compila aplicación con hotreload en dispositivo Android físico o emulador

