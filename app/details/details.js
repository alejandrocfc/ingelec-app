'use strict';
angular.module('details', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider) {

  // ROUTING with ui.router
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('main.details', {
      url: '/details',
      params: {detail: null},
      views: {
        homeView: {
          templateUrl: 'details/templates/detail.html',
          controller: 'DetailCtrl as ctrl'
        }
      }
    });
});
