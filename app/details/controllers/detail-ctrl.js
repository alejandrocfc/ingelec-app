'use strict';
angular.module('details')
.controller('DetailCtrl', function ($stateParams,$scope) {

  var info = JSON.parse($stateParams.detail);

  $scope.info = [
    { label: "Nombre", value: info.nombre_solicitud},
    { label: "Descripción", value: info.descripcion_solicitud},
    { label: "Ticket", value: info.ticket_solicitud},
    { label: "Cliente", value: info.nombre_cliente},
    { label: "Torre", value: info.nombre_torre + " - " + info.number_torre},
    { label: "Posición", value: info.torre_latitud + " , " + info.torre_longitud},
    { label: "Anchor", value: info.torre_anchor_id},
    { label: "Ciudad", value: info.departamento+" , "+info.municipio},
    { label: "Fecha asignación", value: info.fecha_asignacion},
    { label: "Fecha ejecución", value: info.fecha_ejecucion},
  ];
});
