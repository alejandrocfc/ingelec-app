function SkObject(sketchIn){
    if(sketchIn == undefined)
        throw TypeError("SketchIn element argument is mandatory");
    this.sk = sketchIn;
    this.paper = this.sk.s;
    this.name = '';
    this.snapObject = undefined;
    this.editing = false;
}

(function(proto, undefined){
    proto.setColor = function(c){
        this.snapObject.attr({
            stroke: c
        });
		this.onchange();
        return this;
    };
	
    proto.setSizeFactor = function(s){
        this.snapObject.attr({
            strokeWidth:this.sk.lineMultiplier * s * this.sk.pixelRatio
        });
		this.onchange();
        return this;
    };
	
    proto.delete = function(){
		this.onchange();
        this.snapObject.remove();
        this.sk.removeObject(this);
        this.snapObject = null;
    };
	
    proto.stopEdition = function(){
        if(this.snapObject !== undefined)
            this.snapObject.remove();
    };

    proto.edit = function(){
        return false;
    };

    proto.className = function(){
        throw new TypeError("Unimplemented method className");
    };

    proto.move = function(x,y){
        throw new TypeError("Unimplemented method move");
    };
	
	proto.onchange = function(){
		this.sk.onchange();
	};
}(SkObject.prototype));

function SkLine(sketchIn){
    SkObject.call(this, sketchIn);
}

/**
 * Prototype for SkLine
 */
(function(undefined){

    //Inherit
    SkLine.prototype = Object.create(SkObject.prototype);
    SkLine.prototype.constructor = SkLine;

    var proto = SkLine.prototype;
	
	proto.className = function(){
		return "SkLine";
	};

    proto.move = function(x,y){
        this.snapObject.attr({
            x1: parseFloat(this.snapObject.attr('x1')) + x,
            y1: parseFloat(this.snapObject.attr('y1')) + y,
            x2: parseFloat(this.snapObject.attr('x2')) + x,
            y2: parseFloat(this.snapObject.attr('y2')) + y
        });
    };

    /**
     * Mouse event in SVG region
     * On click: start or end line
     */
    proto.action = function(event,x,y){
        switch(event.type){
            case 'mousedown':
            case 'touchstart':
                if(this.editing === false) {
                    this._start(x,y);
                    this.editing = true;
                }
				break;
			case 'mouseup':
            case 'touchend':
                if(this.editing === true) {
                    this.editing = false;
                    this.sk.addObject(this);
                    if((Math.abs(this.snapObject.attr('x1') - this.snapObject.attr('x2')) < 5 * this.sk.pixelRatio)
                        && (Math.abs(this.snapObject.attr('y1') - this.snapObject.attr('y2')) < 5 * this.sk.pixelRatio))
                        this.delete();
                }
                break;
            case 'mousemove':
            case 'touchmove':
                if(this.editing === true)
                    this._draw(x,y);
        }
    };
	
    proto._start = function(startX,startY){
        this.snapObject = this.paper.line(startX, startY, startX, startY);
        if(this.sk.svg_container){
            this.sk.svg_container.add(this.snapObject);
        }
        
        this.snapObject.attr({
            strokeWidth:this.sk.lineMultiplier * this.sk.sizeFactor * this.sk.pixelRatio,
            stroke:this.sk.strokeColor
        });
        return this;
    };
	
    proto._draw = function(finalX, finalY){
        this.snapObject.attr({x2: finalX, y2:finalY});
        return this;
    };
}());


function SkArrow(sketchIn){
    SkObject.call(this, sketchIn);
    this.editing = false;
    this.clicking = false;
    this.points = [];
}
/**
 * Prototype for SkArrow
 */
(function(undefined){
    //Inherit
    SkArrow.prototype = Object.create(SkLine.prototype);
    SkArrow.prototype.constructor = SkArrow;

    var proto = SkArrow.prototype;

    proto.className = function(){
        return "SkArrow";
    };

    proto._start = function(startX,startY){
        var arrow = this.paper.polygon([0,10, 6,10, 3,0, 0,10])
                        .attr({fill: this.sk.strokeColor, orient:'auto'})
                        .transform('r90');
        var marker = arrow.marker();
        this.snapObject = this.paper.line(startX, startY, startX+2*this.sk.pixelRatio, startY);

        if(this.sk.svg_container){
            this.sk.svg_container.add(this.snapObject);
        }

        this.snapObject.attr({
            strokeWidth:this.sk.lineMultiplier * this.sk.sizeFactor * this.sk.pixelRatio,
            stroke:this.sk.strokeColor,
            markerEnd:marker
        });
        return this;
    };

    proto.setColor = function(c){
        this.snapObject.attr({
            stroke: c
        });
        var marker = this.snapObject.attr('markerEnd');
        // Get the SVG object inside the marker
        // Hack to convert the arrowhead polygon into a snap object
        var arrowHead = Snap(marker.node.lastChild);
        arrowHead.attr({
            fill: c
        });
        this.onchange();
        return this;
    };
    proto.delete = function(){
        this.onchange();
        // Delete the marker
        var marker = this.snapObject.attr('markerEnd');
        this.snapObject.attr({markerEnd: null});
        marker.remove();
        marker = null;
        this.snapObject.remove();
        this.sk.removeObject(this);
        this.snapObject = null;
    };
}());


function SkPolyLine(sketchIn){
    SkObject.call(this, sketchIn);
    this.editing = false;
    this.clicking = false;
    this.points = [];
}
/**
 * Prototype for SkPolyLine
 */
(function(undefined){
    //Inherit
    SkPolyLine.prototype = Object.create(SkObject.prototype);
    SkPolyLine.prototype.constructor = SkPolyLine;

    var proto = SkPolyLine.prototype;

	proto.className = function(){
		return "SkPolyLine";
	};
	
    proto.stopEdition = function(){
        if(this.snapObject !== undefined)
            this.sk.addObject(this);
    };
    /**
     * Mouse event in SVG region
     * The first click starts the polyline.
     * Successive clicks adds points to the polyline.
     * Double-click ends editions
     */
    proto.action = function(event,x,y){
        switch(event.type){
            case 'mousedown':
            case 'touchstart':
                this.clicking = true;
                if(this.editing === false) {
                    this._start(x,y);
                    this.editing = true;
                }
				break;
			case 'mouseup':
            case 'touchend':
                this.clicking = false;
				if(this.editing === true) {
                    this._addPoint(this.points[this.points.length-2],this.points[this.points.length-1]);
                }
                break;
            case 'mousemove':
            case 'touchmove':
                if(this.editing && !this.clicking)
                    this._lastPoint(x,y);
                else if(this.editing && this.clicking)
                    this._updateLast(x,y);
                break;
            case 'dblclick':
                this.clicking = false;
                this.editing = false;
                this._addPoint(x,y);
                this.points = [];
                this.sk.addObject(this);
        }
    };
	
    proto._start = function(startX,startY){
        this.points.push(startX);
        this.points.push(startY);
        this.points.push(startX);
        this.points.push(startY);
        this.snapObject = this.paper.polyline(this.points);
        if(this.sk.svg_container){
            this.sk.svg_container.add(this.snapObject);
        }
        this.snapObject.attr({
            strokeWidth:this.sk.lineMultiplier * this.sk.sizeFactor * this.sk.pixelRatio,
            stroke:this.sk.strokeColor,
            fill: 'none'
        });
        this.snapObject.skObject = this;
        return this;
    };
	
    proto._addPoint = function(x, y){
        this.points.push(x);
        this.points.push(y);
        this.snapObject.attr({points: this.points.join(",")});
        return this;
    };
	
    proto._lastPoint = function(x, y){
        this.points.pop();
        this.points.pop();
        this.points.push(x);
        this.points.push(y);
        return this;
    };
	
    proto._updateLast = function(x, y){
        this.points.pop();
        this.points.pop();
        this._addPoint(x,y);
        return this;
    };
}());


function SkFreeDrawing(sketchIn){
    SkObject.call(this, sketchIn);
    this.editing = false;
    this.clicking = false;
    this.points = [];
}
/**
 * Prototype for SkFreeDrawing
 */
(function(undefined){
    //Inherit
    SkFreeDrawing.prototype = Object.create(SkPolyLine.prototype);
    SkFreeDrawing.prototype.constructor = SkFreeDrawing;

    var proto = SkFreeDrawing.prototype;

    proto.className = function(){
        return "SkFreeDrawing";
    };

    /**
     * Mouse event in SVG region
     * If click: start or end line
     * If move and
     */
    proto.action = function(event,x,y){
        switch(event.type){
            case 'mousedown':
            case 'touchstart':
                if(this.editing === false) {
                    this._start(x,y);
                    this.editing = true;
                }
                break;
            case 'mouseup':
            case 'touchend':
                this.editing = false;
                this.points = [];
                this.sk.addObject(this);
                break;
            case 'mousemove':
            case 'touchmove':
                if(this.editing === true)
                    this._addPoint(x,y);
                break;
        }
    };
}());


function SkRect(sketchIn){
    SkObject.call(this, sketchIn);
    this.editing = false;
    this.startPoints = {x:0, y:0};
}
/**
 * Prototype for SkRect
 */
(function(undefined){
    //Inherit
    SkRect.prototype = Object.create(SkObject.prototype);
    SkRect.prototype.constructor = SkRect;

    var proto = SkRect.prototype;

	proto.className = function(){
		return "SkRect";
	};

    proto.move = function(x,y){
        this.snapObject.attr({
            x: parseFloat(this.snapObject.attr('x')) + x,
            y: parseFloat(this.snapObject.attr('y')) + y
        });
    };
	
    /**
     * Mouse event in SVG region
     */
    proto.action = function(event,x,y){
        switch(event.type){
            case 'mousedown':
            case 'touchstart':
                if(this.editing === false) {
                    this._start(x,y);
                    this.editing = true;
                }
                break;
            case 'mouseup':
            case 'touchend':
                if(this.editing === true) {
                    this.editing = false;
                    this.sk.addObject(this);
                    if(Math.abs(this.snapObject.attr('width') < 5 * this.sk.pixelRatio)
                        && Math.abs(this.snapObject.attr('height') < 5 * this.sk.pixelRatio))
                        this.delete();
                }
                break;
            case 'mousemove':
            case 'touchmove':
                if(this.editing === true)
                    this._draw(x,y);
        }
    };
	
    proto._start = function(startX,startY){
        this.snapObject = this.paper.rect(startX, startY, 0, 0);
        if(this.sk.svg_container){
            this.sk.svg_container.add(this.snapObject);
        }
        this.startPoints = {x: startX, y:startY};
        this.snapObject.attr({
            fill:'none',
            strokeWidth:this.sk.lineMultiplier * this.sk.sizeFactor * this.sk.pixelRatio,
            stroke:this.sk.strokeColor
        });
        return this;
    };
	
    proto._draw = function(finalX, finalY){
        var x = 0;
        var y = 0;
        var w = 0;
        var h = 0;
        if(finalX - this.startPoints.x < 0){
            x = finalX;
            w = this.startPoints.x - finalX;
        }
        else {
            x = this.startPoints.x;
            w = finalX - this.startPoints.x;
        }
        if(finalY - this.startPoints.y < 0){
            y = finalY;
            h = this.startPoints.y - finalY;
        }
        else {
            y = this.startPoints.y;
            h = finalY - this.startPoints.y;
        }
        w = (w <= 1 ? 2 : w);
        h = (h <= 1 ? 2 : h);
        this.snapObject.attr({x: x, y: y, width: w, height: h});
        return this;
    };
}());

function SkEllipse(sketchIn){
    SkObject.call(this, sketchIn);
    this.editing = false;
    this.startPoints = {x:0, y:0};
}
/**
 * Prototype for SkEllipse
 */
(function(undefined){
    //Inherit
    SkEllipse.prototype = Object.create(SkObject.prototype);
    SkEllipse.prototype.constructor = SkEllipse;

    var proto = SkEllipse.prototype;

	proto.className = function(){
		return "SkEllipse";
	};

    proto.move = function(x,y){
        this.snapObject.attr({
            cx: parseFloat(this.snapObject.attr('cx')) + x,
            cy: parseFloat(this.snapObject.attr('cy')) + y
        });
    };
	
    /**
     * Mouse event in SVG region
     */
    proto.action = function(event,x,y){
        switch(event.type){
            case 'mousedown':
            case 'touchstart':
                if(!this.editing) {
                    this._start(x,y);
                    this.editing = true;
                }
                break;
            case 'mouseup':
            case 'touchend':
                if(this.editing) {
                    this.editing = false;
                    this.sk.addObject(this);
                    if(Math.abs(this.snapObject.attr('rx') < 5 * this.sk.pixelRatio)
                        && Math.abs(this.snapObject.attr('ry') < 5 * this.sk.pixelRatio))
                        this.delete();
                }
                break;
            case 'mousemove':
            case 'touchmove':
                if(this.editing === true)
                    this._draw(x,y);
        }
    };
	
    proto._start = function(startX,startY){
        this.snapObject = this.paper.ellipse(startX, startY, 0, 0);
        if(this.sk.svg_container){
            this.sk.svg_container.add(this.snapObject);
        }
        this.startPoints = {x: startX, y:startY};
        this.snapObject.attr({
            fill:'none',
            strokeWidth:this.sk.lineMultiplier * this.sk.sizeFactor * this.sk.pixelRatio,
            stroke:this.sk.strokeColor
        });
        return this;
    };
	
    proto._draw = function(finalX, finalY){
        var cx = 0;
        var cy = 0;
        var dx = 0;
        var dy = 0;
        var rx = 0;
        var ry = 0;
		
		dx = (finalX - this.startPoints.x);
		dy = (finalY - this.startPoints.y);
		
		cx = this.startPoints.x + dx/2;
		cy = this.startPoints.y + dy/2;
		
		rx = Math.abs(dx/2);
		ry = Math.abs(dy/2);
		
        this.snapObject.attr({cx: cx, cy: cy, rx: rx, ry: ry});
        return this;
    };
}());

function SkText(sketchIn){
    SkObject.call(this, sketchIn);
    this.text = "";
    this.point = {x:0, y:0};
    //this.editing = false;
    if(!this.editing){
        this.editing = false;
    }
}
/**
 * Prototype for SkText
 */
(function(undefined){
    //Inherit
    SkText.prototype = Object.create(SkObject.prototype);
    SkText.prototype.constructor = SkText;

    var proto = SkText.prototype;

	proto.className = function(){
		return "SkText";
	};

    proto.move = function(x,y){
        this.snapObject.attr({
            x: parseFloat(this.snapObject.attr('x')) + x,
            y: parseFloat(this.snapObject.attr('y')) + y
        });
        this.point = {
            x: parseFloat(this.snapObject.attr('x')),
            y: parseFloat(this.snapObject.attr('y'))
        };
    };
	
    proto.setColor = function(c){
        this.snapObject.attr({
            fill: c
        });
		this.onchange();
        return this;
    };
	
    proto.setSizeFactor = function(s){
        this.snapObject.attr({
            'font-size': ((this.sk.fontMin + (this.sk.fontMultiplier * s)) * this.sk.pixelRatio) + 'px'
        });
		this.onchange();
        return this;
    };
	
    proto.stopEdition = function(){
        if(this.text)
            this._makeText();
    };

    /**
     * Mouse event in SVG region
     */
    proto.action = function(event,x,y){
        switch(event.type){
            case 'mouseup':
            case 'touchstart':
                if(!this.editing) {
                    this._start(x,y);
                    this.editing = true;
                }else {
                    this._makeText();
                }
                break;
        }
    };
	
    proto._start = function(startX,startY,text){
        if(text === undefined)
            text = "";
        this.point = {x: startX, y: startY};
        this.text = prompt('Enter text you want to add', text);
		this.stopEdition();
        return this;
    };
	
    proto.edit = function(){
        var x = this.point.x;
        var y = this.point.y;
        this.editing = true;
        this.snapObject.remove();
        this.snapObject = null;
        this._start(x, y-6, this.text);
        return this;
    };
	
    proto._makeText = function(){
        this.editing = false;
        if(this.text && this.text != ""){
            this.snapObject = this.paper.text(this.point.x, this.point.y, this.text);
            if(this.sk.svg_container){
                this.sk.svg_container.add(this.snapObject);
            }
            this.snapObject.attr({
                'fontSize': ((this.sk.fontMin + (this.sk.fontMultiplier * this.sk.sizeFactor)) * this.sk.pixelRatio) + 'px',
                fill: this.sk.strokeColor
            });
            this.sk.addObject(this, null, true);
        }

        return this;
    };
}());

function SkImage(sketchIn){
    SkObject.call(this, sketchIn);
    this.image = {src:'', base64:'', w:0, h:0};
    this.point = {x:0, y:0};
    this.editing = false;
}
/**
 * Prototype for SkImage
 */
(function(undefined){
    //Inherit
    SkImage.prototype = Object.create(SkObject.prototype);
    SkImage.prototype.constructor = SkImage;

    var proto = SkImage.prototype;

	proto.className = function(){
		return "SkImage";
	};
	
    proto.move = function(x,y){
        this.snapObject.attr({
            x: parseFloat(this.snapObject.attr('x')) + x,
            y: parseFloat(this.snapObject.attr('y')) + y
        });
        this.point = {
            x: parseFloat(this.snapObject.attr('x')),
            y: parseFloat(this.snapObject.attr('y'))
        };
    };
	
    proto.setSizeFactor = function(s){
		this.snapObject.attr({
            'width': this.snapObject.attr('orig_width') * ( s / ( this.sk.sizeOptions * 4 ) ) * this.sk.pixelRatio,
            'height': this.snapObject.attr('orig_height') * ( s / ( this.sk.sizeOptions * 4 ) ) * this.sk.pixelRatio,
        });
		this.onchange();
        return this;
    };
	
    proto.stopEdition = function(){
        if(this.image.base64)
            this._makeImage();
    };

    /**
     * Mouse event in SVG region
     */
    proto.action = function(event,x,y){
        switch(event.type){
            case 'mouseup':
            case 'touchstart':
                if(!this.editing) {
                    this._start(x,y);
                    this.editing = true;
                }
                else {
                    this._makeImage();
                }
                break;
        }
    };
	
	/**
	 * Convert an image 
	 * to a base64 string
	 * @param  {String}   url         
	 * @param  {Function} callback    
	 * @param  {String}   [outputFormat=image/jpg]           
	 */
	proto._convertImgToBase64 = function(outputFormat){
		var canvas = document.createElement('CANVAS'),
			ctx = canvas.getContext('2d'),
			img = new Image(),
			obj = this;
		img.crossOrigin = 'Anonymous';
		img.onload = function(){
			var dataURL;
			canvas.height = img.height;
			canvas.width = img.width;
			ctx.drawImage(img, 0, 0);
			dataURL = canvas.toDataURL(outputFormat || 'image/jpg');
			canvas = null;
			obj.image = {src: obj.sk.imageSrc, base64: dataURL, w: img.width, h: img.height};
			obj.stopEdition();
		};
		img.src = obj.sk.imageSrc;
	};
	
    proto._start = function(startX,startY){
        this.point = {x: startX, y: startY};
        this._convertImgToBase64();
        return this;
    };
	
    proto.edit = function(){
        var x = this.point.x;
        var y = this.point.y;
        this.editing = true;
        this.snapObject.remove();
        this.snapObject = null;
        this._start(x,y);
        return this;
    };
	
    proto._makeImage = function(){
        this.editing = false;
		var w = this.image.w * ( this.sk.sizeFactor / ( this.sk.sizeOptions * 4 ) ) * this.sk.pixelRatio;
		var h = this.image.h * ( this.sk.sizeFactor / ( this.sk.sizeOptions * 4 ) ) * this.sk.pixelRatio;
        this.snapObject = this.paper.image(this.image.base64, this.point.x - (w/2), this.point.y - (h/2), w, h);
		this.snapObject.attr({
            'orig_width': this.image.w,
            'orig_height': this.image.h,
        });
        if(this.sk.svg_container){
            this.sk.svg_container.add(this.snapObject);
        }
        this.sk.addObject(this);
        return this;
    };
}());
