
function SketchIn(svgObject, realWidth, realHeight, onchangeCallback, containerWidth, containerHeight){
    // Polyfill for getTransformToElement as Chrome 48 has deprecated it, may be able to simplify globalToLocal now and leave out polyfill 
    SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(elem) {
        return elem.getScreenCTM().inverse().multiply(this.getScreenCTM());
    };

    Snap.plugin( function( Snap, Element, Paper, global ) {
        Element.prototype.addTransform = function( t ) {
            return this.transform( this.transform().localMatrix.toTransformString() + t );
        };

        Element.prototype.globalToLocal = function( globalPoint ) {
            var globalToLocal = this.node.getTransformToElement( this.paper.node ).inverse();
            globalToLocal.e = globalToLocal.f = 0;
            return globalPoint.matrixTransform( globalToLocal );
        };

        Element.prototype.getCursorPoint = function( x, y ) {
                var pt = this.paper.node.createSVGPoint();      
                pt.x = x; pt.y = y;
                return pt.matrixTransform( this.paper.node.getScreenCTM().inverse()); 
        }

        Element.prototype.updateLimit = function(params){
            this.data('dragParams', params)
        }

        Element.prototype.limitDrag = function( params ) {
                this.data('dragParams', params );
                return this.drag( limitMoveDrag, limitStartDrag );
        };

        Element.prototype.removeAttr = function( attr ) {
            return this.node.removeAttribute( attr );
        };

        function limitMoveDrag( dx, dy, ax, ay ) {
                var params = this.data('dragParams');
                var cursorPoint = this.getCursorPoint( ax, ay );
                var pt = this.paper.node.createSVGPoint();
                var ibb = this.data('ibb');

                pt.x = cursorPoint.x - this.data('op').x;
                pt.y = cursorPoint.y - this.data('op').y;

                if( ibb.x2 + pt.x > params.maxx ) { pt.x = params.maxx - ibb.x2; }
                if( ibb.y2 + pt.y > params.maxy ) { pt.y = params.maxy - ibb.y2; }
                if( ibb.x + pt.x < params.minx ) { pt.x = params.minx - ibb.x; }
                if( ibb.y + pt.y < params.miny ) { pt.y = params.miny - ibb.y; }
        
                var localPt = this.globalToLocal( pt );

                this.transform( this.data('ot').toTransformString() + "t" + [  localPt.x, localPt.y ] );
        };

        function limitStartDrag( x, y, ev ) {
                this.data('ibb', this.getBBox()); 
                this.data('op', this.getCursorPoint( x, y ));
                this.data('ot', this.transform().localMatrix);
        };
    });

    // Save 'this' context
    var self = this;

    self.lastMove = null;

    // Select is always the default tool.
    this.containerWidth = containerWidth;
    this.containerHeight = containerHeight;
    this.scale = 1;
    this.rotation = 0;
    
    //zoom tools
    this.svg_container = null;
    this.drag_limits = null;
    this.viewBox = null;
    this.dragInitialized = false;
    //end zoom tools

    //end rotation tools
    this.tool = 'select';
    this.objects = {};
    this.activeObject = undefined;
    this.selectedObject = undefined;
    this.highlightedObject = undefined;
    this.hBBox = undefined;
    this.sBBox = undefined;
    this.s = Snap(svgObject);
    this.s.transform('S1,1,T0,0');
    
    // Real attributes
    this.width = realWidth;
    this.height = realHeight;
    this.s.node.setAttribute('viewBox', '0 0 ' + this.width + ' ' + this.height);
    this.viewBox = self.s.attr("viewBox");
    this.svgWidth = parseFloat(this.s.node.getAttribute('width'));
    this.svgHeight = parseFloat(this.s.node.getAttribute('height'));

    this.pixelRatio = Math.sqrt((this.width * this.height) / (this.svgWidth * this.svgHeight));
    
    // Background image object
    this.bgImage = undefined;
    this.bgImageInit = undefined;
    this.bgImageInitWidth = realWidth;
    this.bgimageInitHeight = realHeight;

    // Current properties defaults
    this.strokeColor = "#F00000";
	this.sizeFactor = 1;
	this.sizeOptions = 3;
	this.lineMultiplier = 1;
	this.fontMultiplier = 3;
	this.fontMin = 10;
    this.imageSrc = "";

    // Export parameters
    this.EXPORT_WIDTH = 640;
    this.EXPORT_HEIGHT = 480;
    this.base64 = false;

    // Callbacks
    this.onchangeCallback = onchangeCallback != undefined ? onchangeCallback : undefined;

    this._setContext();
    
    self.removeEvents = function(){
        self.s.untouchstart(self.actionHandler);
        self.s.untouchend(self.actionHandler);
        self.s.unmousedown(self.actionHandler);
        self.s.unmouseup(self.actionHandler);
        self.s.undblclick(self.actionHandler);
        self.s.unmousemove(self.actionHandler);
        self.s.untouchmove(self.actionHandler);
    }

    self.addEvents = function(){
        self.s.touchstart(self.actionHandler);
        self.s.touchend(self.actionHandler);
        self.s.mousedown(self.actionHandler);
        self.s.mouseup(self.actionHandler);
        self.s.dblclick(self.actionHandler);
        self.s.mousemove(self.actionHandler);
        self.s.touchmove(self.actionHandler);
    }
	// Set desc and load elements
	var desc = this.s.node.getElementsByTagName("desc")[0];
	desc.innerHTML = "SketchIn v1.0";
	
	this.s.selectAll("*").forEach(function(el){
		// Remove any unidentified object that is no defs or desc or descendents
        var parent = el;
        while(parent && parent.type != 'svg')
        {
    		if(parent.type == 'defs' || parent.type == 'desc')
    			return true;

            if(parent.attr('id') == 'sk_date_text' || parent.attr('id') == 'sk_date_text_shadow')
                return true;
            
            parent = parent.parent();
        }
		if(!el.attr('id')){
			el.remove();
			return true;
		}
        // We skip if it's the BG image
        if(el.type == 'g' && el.attr('id') == 'svg_container'){
            return true;
        }
		if(el.type == 'image' && el.attr('id') == 'bgImage')
			return true;
		var skObj = el.attr('id').split("_")[0];
		// Remove the element if we can't identify it
		if((typeof window[skObj] != 'function')){
			el.remove();
			return true;
        }
		this.setTool(window[skObj]);
		this.activeObject.snapObject = el;
		this.addObject(this.activeObject, true);
	}, this);
	
    // Load background image if any
    this.svg_container = this.s.select('#svg_container');
    if(this.svg_container){
        this.bgImage = this.svg_container.select("#bgImage") ? this.s.select("#bgImage") : undefined;
    }else {
        this.bgImage = this.s.select("#bgImage") ? this.s.select("#bgImage") : undefined;
    }

    
    if(!this.svg_container){
        var items = this.s.selectAll('*').items;
        if(items){
            this.svg_container = this.s.group();
            this.svg_container.attr({id:'svg_container'});
            
            for(var i = 0; i<items.length; i++){
                var clone = items[i];
                this.svg_container.append(clone);
            }
        }
    }

    if(this.bgImage){
        this.bgImageInit = {src:this.bgImage.node.href.baseVal, width: this.bgImage.node.width.baseVal.value, height: this.bgImage.node.height.baseVal.value };
    }else{
        var rect = this.s.rect(0,0, realWidth, realHeight);
        rect.attr({id: 'empty_bg', fill: 'white'});
        this.svg_container.prepend(rect);
    }
    self.g_width = this.svg_container.getBBox().width > 0 ? this.svg_container.getBBox().width : this.svgWidth;
    self.g_height = this.svg_container.getBBox().height > 0 ? this.svg_container.getBBox().height : this.svgHeight;
    // Bind click on SVG area
    
    self.addEvents();

    // Bind keypress
    document.onkeydown = function(event){
        event = event || window.event;
        self.keysHandler(event);
	}
	
	// Go back to standard select tool
	this.setTool('select');
}
function in_array(what, where)
{
  for(i=0; i<where.length; i++)
  {
    if (where[i] == what)
      return true;
  }

  return false;
}

(function(proto, undefined){

    var self = undefined;

    proto.addObject = function(svgObject, startup, addEvents){
        if(startup == undefined)
            startup = false;
        if(!svgObject.snapObject) return;
        this.objects[svgObject.snapObject.id] = svgObject;
        svgObject.snapObject.attr({id:svgObject.className() + "_" + svgObject.snapObject.id});
        if(addEvents){
            this.removeEvents();
            this.activeObject = new self.tool(this);
            setTimeout(function(){ // prevent add multiple text windows
                self.addEvents();
            }, 500);
        }else {
            this.activeObject = new self.tool(this);
        }
        
        if(!startup)
            this.onchange();
    }

    proto.removeObject = function(svgObject){
        delete this.objects[svgObject.snapObject.id];
		this._removeHighlights();
        this.onchange();
    }

    proto.onchange = function()
    {
        if(typeof this.onchangeCallback == 'function')
            this.onchangeCallback();
    }

    proto._setContext = function(){
        self = this;
    }

    proto.setTool = function(tool){
        this._removeHighlights();
        // Remove ongoing object if any
        if(tool != 'zoomIn' && tool != 'zoomOut' && self.svg_container){
            self.svg_container.undrag();
            this.dragInitialized = false;
        }
        if(this.activeObject != undefined)
            this.activeObject.stopEdition();
        if(typeof tool != "function"){
            this.activeObject = undefined;
            this.tool = tool;
        }
        else {
            this.activeObject = new tool(this);
            this.tool = tool;
        }
    }

    proto.setColor = function(c){
        if(this.selectedObject !== undefined)
            this.selectedObject.setColor(c);
        this.strokeColor = c;
    }

    proto.setSizeFactor = function(s){
        if(this.selectedObject !== undefined)
            this.selectedObject.setSizeFactor(s);
        this.sizeFactor = s;
    }
	
    proto.setImageSrc = function(src){
        this.imageSrc = src;
    }

    proto.loadBgImage = function(base64Img, w, h){
        
        this.prepareSave();
        this.width = w;
        this.height = h;
        resizeSVG(w,h);
        this.s.node.setAttribute('viewBox', '0 0 ' + w + ' ' + h);
        this.viewBox = this.s.attr("viewBox");
        this.origTransform = this.s.transform().local;
        this.svgWidth = parseFloat(this.s.node.getAttribute('width'));
        this.svgHeight = parseFloat(this.s.node.getAttribute('height'));
        this.pixelRatio = Math.sqrt((this.width * this.height) / (this.svgWidth * this.svgHeight));
        self.scale = 1;
        self.dragInitialized = false;
        
        if(this.bgImage !== undefined){
            this.bgImage.remove();
        }
            
        this.bgImage = base64Img ? this.s.image(base64Img, 0, 0, w, h) : undefined;
        
        if(this.bgImage){
            this.bgImage.attr({id: "bgImage"});
            if(!this.bgImageInit){
                this.bgImageInit = {src:this.bgImage.node.href.baseVal, width: this.bgImage.node.width.baseVal.value, height: this.bgImage.node.height.baseVal.value };
            }
            // Send to back (first child of SVG element);
            if(self.svg_container){
                this.bgImage.prependTo(self.svg_container);
            }else {
                this.bgImage.prependTo(this.s);
            }
        }else{
            var rect = this.s.rect(0,0, w, h);
            rect.attr({id: 'empty_bg', fill: 'white'});
            this.svg_container.prepend(rect);
        }
        //group width
        self.g_width = this.svg_container.getBBox().width > 0 ? this.svg_container.getBBox().width : w;
        self.g_height = this.svg_container.getBBox().height > 0 ? this.svg_container.getBBox().height : h;

        // Call change callback on change
        this.onchange();
    }

    proto.rotate = function() {
        this.resize();
        this.rotation = (this.rotation + 90) % 360;
        var canvas = document.createElement('canvas');
        this.svgWidth = parseFloat(this.s.node.getAttribute('width'));
        this.svgHeight = parseFloat(this.s.node.getAttribute('height'));
        
        var hour_text_shadow = this.s.select('#sk_date_text_shadow');
        var hour_text = this.s.select('#sk_date_text');
        if(!this.bgImageInit){
            $.each(self.objects, function(k, o) {
                o.delete();
            });
            var w, h;
            if(this.rotation == 90 || this.rotation == 270){
                w = this.bgimageInitHeight;
                h = this.bgImageInitWidth;
            }else {
                w = this.bgImageInitWidth;
                h = this.bgimageInitHeight;
            } 
            if(hour_text_shadow){
                hour_text_shadow.attr('x', w - 184);
                hour_text_shadow.attr('y', h - 13);
            }
            if(hour_text){
                hour_text.attr('x', w - 185);
                hour_text.attr('y', h - 14);
            }
            self.loadBgImage(undefined, w, h);  
            return;
        }
        
        if(this.rotation == 90 || this.rotation == 270){
            canvas.width = this.bgImageInit.width;
            canvas.height = this.bgImageInit.height;
        }else {
            canvas.width = this.bgImageInit.height;
            canvas.height = this.bgImageInit.width;
        }

        // Get current rotation
        var ctx = canvas.getContext("2d");
        var img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
            drawRotated(canvas, ctx, img, self.rotation);
            var img_str = canvas.toDataURL('image/jpeg', 0.85);
            $.each(self.objects, function(k, o) {
                o.delete();
            });
            if(hour_text_shadow){
                hour_text_shadow.attr('x', canvas.width - 184);
                hour_text_shadow.attr('y', canvas.height - 13);
            }
            if(hour_text){
                hour_text.attr('x', canvas.width - 185);
                hour_text.attr('y', canvas.height - 14);
            }
            
            self.loadBgImage(img_str, canvas.width, canvas.height);
              
        };
        img.src = this.bgImageInit.src;
        
    };

    proto.resize = function(){
        this.svgWidth = parseFloat(this.s.node.getAttribute('width'));
        this.svgHeight = parseFloat(this.s.node.getAttribute('height'));
        var w = this.svgHeight;
        var h = this.svgWidth;
        
        if(this.rotation == 90 || this.rotation == 270){
            if(this.svgWidth < this.svgHeight){
                var diff = this.containerWidth / w;
                w = this.svgHeight * diff;
                h = this.svgWidth * diff;
            }else {
                var diff = this.containerHeight / h;
                w = this.svgHeight * diff;
                h = this.svgWidth * diff;
            }
            
        }else {
            
            if(this.svgWidth < this.svgHeight){
                var diff = this.containerWidth / w;
                w = this.svgHeight * diff;
                h = this.svgWidth * diff;
            }else {
                var diff = this.containerHeight / h;
                w = this.svgHeight * diff;
                h = this.svgWidth * diff;
            }
            
            
        }

        this.s.node.setAttribute('width', w);
        this.s.node.setAttribute('height', h);
    }
    
    function resizeSVG(w,h){
        gW = w;
        gH = h;

        var sW = w;
        var sH = h;

        var cW = self.containerWidth;
        var cH = self.containerHeight;

        if (sW > cW || sH > cH)
        {
            if (sW/sH > cW/cH)
            {
                sH = cW / (sW / sH);
                sW = cW;
            }
            else
            {
                sW = cH * (sW / sH);
                sH = cH;
            }
        }
        
        var svg = $('#svg').get(0);
        svg.setAttribute('width', sW);
        svg.setAttribute('height', sH);
    }

    function drawRotated(canvas, context, image, degrees){
        
        cw = canvas.width;
        ch = canvas.height;
        // save the unrotated context of the canvas so we can restore it later
        // the alternative is to untranslate & unrotate after drawing
        context.clearRect(0,0,canvas.width,canvas.height);
        
        
        canvas.width = ch;
        canvas.height = cw;
        context.save();

        // move to the center of the canvas
        context.translate(canvas.width/2,canvas.height/2);
    
        // rotate the canvas to the specified degrees
        context.rotate(degrees*Math.PI/180);
    
        // draw the image
        // since the context is rotated, the image will be rotated also
        context.drawImage(image,-image.width/2,-image.height/2);
    
        // we’re done with the rotating so restore the unrotated context
        context.restore();
    }

    proto.zoomIn = function(){
        if(this.scale < 5){
            this.scale += .2;
            this.scaling = false;
            zoom();
        }
    }

    proto.zoomOut = function(){
        if(this.scale > 1){
            this.scaling = false;
            this.scale -= .2;
            zoom();
        }
    }

    proto.prepareSave = function(){
        var rect = self.s.select('#empty_bg');
        if(rect){
            rect.remove();
        }
        self.svg_container.undrag();
        self.svg_container.transform("T0,0,S1,1");
        self.dragInitialized = false;
    }

    proto.resetZoom = function(){
        self.scale = 1;
        zoom();
    }

    function zoom(){
        var pos_x = 0;
        var pos_y = 0;
        var t = "S"+self.scale+","+self.scale+","+"T"+pos_x+","+pos_y;
        self.svg_container.transform(t);
        dLimit();
    }

    function dLimit(){
        
        var canvas_width = self.viewBox.width;
        var canvas_height = self.viewBox.height;
        var g_width = self.g_width;
        var g_height = self.g_height;
        var g_scaled_width = (self.scale * g_width);
        var g_scaled_height = (self.scale * g_height);
        var g_scaled_width_diff = g_scaled_width - g_width;
        var g_scaled_height_diff = g_scaled_height -g_height;
        var min_x = g_scaled_width > canvas_width ? -g_scaled_width_diff : 0;
        var max_x = g_scaled_width > canvas_width ? g_scaled_width : g_width;
        var min_y = g_scaled_height > canvas_height ? -g_scaled_height_diff : 0;
        var max_y = g_scaled_height > canvas_height ? g_scaled_height : g_height;
        if(self.dragInitialized){
            self.svg_container.updateLimit({ x: 0, y: 0, minx: min_x, miny: min_y, maxx: max_x, maxy: max_y });
        }else {
            self.dragInitialized = true;
            self.svg_container.limitDrag({ x: 0, y: 0, minx: min_x, miny: min_y, maxx: max_x, maxy: max_y });    
        }
        
    }

    /**
     * Retrieve the coordinates of the given event relative to the center
     * of the widget.
     * source: http://acko.net/blog/mouse-handling-and-absolute-positions-in-javascript/
     *
     * @param event
     *   A mouse-related DOM event.
     * @return
     *    A hash containing keys 'x' and 'y'.
     */
    proto.getAbsolutePosition = function(element) {
        var r = { x: element.offsetLeft, y: element.offsetTop };
        if (element.offsetParent) {
            var tmp = this.getAbsolutePosition(element.offsetParent);
            r.x += tmp.x;
            r.y += tmp.y;
        }
        return r;
    };

    proto.getRelativeCoordinates = function(event) {
        var reference = this.s.node.parentNode;
        var x, y;
        event = event || window.event;
        var eventType = event.type;
        if(eventType == 'touchstart' || eventType == 'touchmove'){
            if(event.touches){
                event = event.touches[0];
            };
            self.lastMove = event;
        }if(event.type == 'touchend'){
            event = self.lastMove;
        }

        // Use absolute coordinates
        var pos = this.getAbsolutePosition(reference);
        x = event.pageX  - pos.x;
        y = event.pageY - pos.y;

        // Calculate positions according to viewbox
        var viewBoxAttrs = self.s.node.getAttribute('viewBox').split(' ');
        var viewWidth = parseFloat(viewBoxAttrs[2]);
        var viewHeight = parseFloat(viewBoxAttrs[3]);
        
        var m = self.svg_container.attr('transform').localMatrix;
        
        var mX;
        var mY;
        var end_x;
        var end_y;

        mX = viewWidth / self.svgWidth;
        mY = viewHeight / self.svgHeight;
        end_x = ((x * mX) / self.scale) - (m.e / self.scale);
        end_y = ((y * mY) / self.scale) - (m.f / self.scale);
        
        if(end_x < 0){
            end_x = 0;
        }
        if(end_x > self.g_width){
            end_x = self.g_width;
        }
        if(end_y < 0){
            end_y = 0;
        }
        if(end_y > self.g_height){
            end_y = self.g_height;
        }
        return { x: end_x, y:  end_y};
    }

    proto._select = function(){
        var vObject = self.highlightedObject;
        if(!vObject && self.sBBox !== undefined){
            self.sBBox.remove();
            self.sBBox = undefined;
            self.selectedObject = undefined;
            self.highlightedObject = undefined;
            return this;
        }
        if(!vObject)
            return this;
        var bBox = vObject.snapObject.getBBox();
        var strokeWidth = parseFloat(vObject.snapObject.attr('strokeWidth'));
        bBox = self.s.rect(bBox.x-strokeWidth, bBox.y-strokeWidth, bBox.width+(2*strokeWidth), bBox.height+(2*strokeWidth));

        if(self.selectedObject != undefined)
            self.sBBox.remove();
        self.selectedObject = vObject;
        self.highlightedObject = undefined;
        self.sBBox = bBox;
        bBox.attr({
            stroke: "#111",
            strokeWidth: 0.5 * self.pixelRatio,
            fill: 'none',
            'stroke-dasharray': 2 * self.pixelRatio + "," + 4 * self.pixelRatio
        });
        return this;
    }

    proto._highlight = function(vObject){
        if(self.hBBox !== undefined){
            self.hBBox.remove();
            self.hBBox = undefined;
            self.highlightedObject = undefined;
        }
        if(!vObject)
            return this;
        self.highlightedObject = vObject;
        var bBox = vObject.snapObject.getBBox();
        var strokeWidth = parseFloat(vObject.snapObject.attr('strokeWidth'));
        bBox = self.s.rect(bBox.x-strokeWidth, bBox.y-strokeWidth, bBox.width+(2*strokeWidth), bBox.height+(2*strokeWidth));

        self.hBBox = bBox;
        bBox.attr({
            stroke: "#009",
            strokeWidth: 0.5 * self.pixelRatio,
            fill: 'none',
            'stroke-dasharray': 2 * self.pixelRatio + "," + 4 * self.pixelRatio
        });
        return this
    }
    proto._removeHighlights = function(){
        if(self.hBBox !== undefined)
            self.hBBox.remove();
        if(self.sBBox !== undefined)
            self.sBBox.remove();
        self.selectedObject = undefined;
    }
    proto._startMove = function(ev, vObj, x, y){

        if(vObj && !this.moving){
            this.movePos = {x: x, y: y};
            this.moveObj = vObj;
            this.moving = true;
        }
    }
    proto._doMove = function(ev, vObj, x, y){
       
        if(this.moving){
            this.moveObj.move(x - this.movePos.x, y - this.movePos.y);
            this.movePos = {x: x, y: y};
        }
        
    }
    proto._endMove = function(){
        if(this.moving){
            this.moving = false;
            this.onchange();
        }
    }

    // Touch Point cache
    this.scaling = false;
    this.dist = null;

    // Click action is passed over to object
    proto.actionHandler = function(event, x, y){
        
        event.preventDefault();

        if(event.originalEvent != undefined)
            event.type = event.originalEvent.type;
        var relPos = self.getRelativeCoordinates(event);
        
        if(x && y && (hoverObject = Snap.getElementByPoint(x, y)))
            var vObject = self.objects[hoverObject.id];
            
        switch(self.tool){
            case 'select':
                // If selecting, mark the element on click and highlight on hover.
                switch(event.type){
                    case 'mouseup':
                    case 'touchend':
                        self._endMove();
                        self._select();
                        break;
                    case 'mousedown':
                    case 'touchstart':
                        self._startMove(event,vObject, relPos.x, relPos.y);
                    case 'mousemove':
                    case 'touchmove':
                        self._highlight(vObject);
                        self._doMove(event, vObject, relPos.x, relPos.y);
                        break;
                }
                
                break;
            case 'zoomIn':
                break;
            case 'zoomOut':
                break;
            default:
                // Having a tool selected and clicking on an object of the
                // same type, tries to edit that object
                if(in_array(event.type, ['mouseup','touchend']) &&
                    vObject &&
                    vObject.className() == self.activeObject.className() &&
                    vObject.edit()){
                    if(self.activeObject)
                        self.activeObject.stopEdition();
                    self.activeObject = vObject;
                }
                else if(self.activeObject != undefined){
                    self.activeObject.action(event, relPos.x, relPos.y);
                }
        }
    }
    proto.keysHandler = function(event){
        switch(event.keyCode){
            case 46: // Delete key
                if(self.selectedObject !== undefined){
                    self.selectedObject.delete();
                }
                self._removeHighlights();
                break;
        }
    }

    proto.getTotalObjects = function(){
        var size = 0, key;
        
        for (key in this.objects) {
            if (this.objects.hasOwnProperty(key)) size++;
        }
        return size;
    }
    
    /**
     *
     * @param callback(b64)
     *  A callback function that is called after the image is rendered into base64 png by default.
     *  The base64 string is passed as an argument to the callback.
     * @param [w]
     *  Optional: width of canvas. If not passed, the object's EXPORT_WIDTH will be used
     * @param [h]
     *  Optional: height of canvas. If not passed, the object's EXPORT_HEIGHT will be used
     * @param [type]
     *  Optional: MIME type of image
     * @returns {*}
     */
    proto.getImage = function(callback, w, h, type){
        if(typeof callback != "function")
            throw TypeError("Callback is not a function");
        w = w != undefined ? w : this.EXPORT_WIDTH;
        h = h != undefined ? h : this.EXPORT_HEIGHT;
        type = type != undefined ? type : 'image/jpg';
        var serializer = new XMLSerializer();
        var svg = serializer.serializeToString(this.s.node);
        var canvas = document.createElement('canvas');
        canvas.setAttribute("id", "exportCanvas");
        canvas.setAttribute("style", "visibility:hidden;");
        canvas.setAttribute("width", w);
        canvas.setAttribute("height", h);
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(canvas);

        this.base64 = false;
        canvg(canvas, svg, {renderCallback: function(){
            callback(canvas.toDataURL(type));
            body.removeChild(canvas);
        },
            log: true, ignoreMouse: true, ignoreAnimation: true, ignoreClear: true, ignoreDimensions: true, scaleWidth:w, scaleHeight:h});
        return this;
    }
    
}(SketchIn.prototype));
