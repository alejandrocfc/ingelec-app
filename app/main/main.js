'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
    }

    /**
     * Disable swipe back
     */
    $ionicConfigProvider.views.swipeBackEnabled(false);
    /**
     * Back button config
     */
    $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-left');
    $ionicConfigProvider.backButton.previousTitleText(false).text('');

    // ROUTING with ui.router
    $urlRouterProvider.otherwise('/main');
    $stateProvider
      .state('main', {
        url: '/main',
        templateUrl: 'main/templates/menu.html',
        controller: 'MenuCtrl as ctrl',
        cache: false
      });
  })
  .run(function ($state, $ionicPlatform, $message, $sqldb, $user, $obras) {

    $ionicPlatform.ready(function () {
      if(ionic.Platform.platform() == "ios")
        navigator.splashscreen.hide();
      else
        setTimeout(function () { navigator.splashscreen.hide(); }, 1000);
      INIT()
    });

    function INIT() {
      $sqldb.dataBase = window.sqlitePlugin.openDatabase({name: 'sinpre.db', location: 'default'});
      $sqldb.checkDB()
        .then(function (resp) {
          getAuth();
        })
        .catch(function (err) {
          $message.popup("Error", err);
        });
    }

    function getAuth(){
      $user.getAuth().then(function (idUser) {
        $obras.byUser(idUser).then(function () {
          $state.go("main.home");
        }).catch(function () {
          $state.go("register");
        });
      }).catch(function () {
        $state.go("register");
      });
    }

  });
