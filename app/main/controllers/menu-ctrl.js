'use strict';
angular.module('main').controller('MenuCtrl', function ($state, $user, $log, $ionicHistory) {

  var ctrl = this;

  ctrl.logout = function () {
    $user.logout().then(function () {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('register');
    })
  };

  ctrl.syncDB = function () {
  }

});
