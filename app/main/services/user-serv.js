'use strict';
angular.module('main')
  .service('$user', function ($q,$http,$sqldb,$network,crypt,Config) {
    var $user = {};

    $user.getAuth = function() {
      return $q(function (resolve, reject) {
        $sqldb.dataBase.transaction(function(tx) {
          tx.executeSql('SELECT * FROM tecnicos WHERE estado=? LIMIT 1', [1], function (tx, results) {
            if( results.rows.item(0) ) {
              var idUsuario = results.rows.item(0).id;
              resolve(idUsuario)
            }else{
              reject()
            }
          }, function (err) {
              reject(err)
          });
        });
      })
    };

    $user.login = function (email, password) {
      return $q(function (resolve, reject) {
        //Si hay conexión se hace el login con el api, sino se hace con el sqlite
        if($network.checkConnection()){
          //user: "diegopaz1988@gmail.com",
          //pass: "I1081400730-0",
          var data = {
            user: email,
            pass: password,
            key: crypt.sha1("*"+Config.key+""+Config.date+"*"),
            login: crypt.sha1(crypt.sha1(Config.login))
          };

          $http.post(Config.baseUrl+"login", data).then(function(response) {
            if(response.data.status === 1) {
              var info = response.data.data;
              $sqldb.dataBase.transaction(function(tx) {
                  tx.executeSql("INSERT OR REPLACE INTO tecnicos (id, nombre, identificacion, clave, email, estado) VALUES (?,?,?,?,?,?)",
                    [info.idUsuario,info.nombre,info.identificacion,password,info.email,1]);
                },
                function(error) {
                  reject(error.message)
                },
                function(){
                  $user.idUsuario = info.idUsuario;
                  resolve(info.idUsuario);
                });
            }else reject(response.data.msj);
          }, function(error) {
            reject(error)
          });
        }
        else{
          $sqldb.dataBase.transaction(function(tx) {
            tx.executeSql("SELECT * FROM tecnicos WHERE email = ? AND clave = ?", [email,password], function (tx, results) {
              if( results.rows.length > 0 ) {
                $user.updateUser(results.rows.item(0))
                  .then(function (res) {
                    resolve()
                  })
                  .catch(function (err) {
                    reject(err)
                  })
              }else{
                reject("Usuario no existe. Por favor verifique su conexión")
              }
            }, function (error) {
              reject(error.message)
            });
          });
        }
      })
    };

    $user.updateUser = function(user){
      return $q(function (resolve, reject) {
        $sqldb.dataBase.transaction(function(tx) {
          tx.executeSql("UPDATE tecnicos SET estado=? WHERE id=?", [1,user.id],
            function(tx, result) {
              $user.idUsuario = user.id;
              resolve()
            },
            function(error){reject(error.message)});
        });
      })
    };

    $user.logout = function () {
      return $q(function (resolve, reject) {
        $sqldb.dataBase.transaction(function(tx) {
          tx.executeSql("UPDATE tecnicos SET estado=? WHERE id=?", [0,$user.idUsuario],
            function(tx, result) {resolve()},
            function(error){reject(error.message)});
        });
      })
    };

    return $user;
  });
