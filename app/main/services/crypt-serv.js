'use strict';
angular.module('main')
  .factory('crypt', function () {
    return {
      hash: function (value) {
        var str = JSON.stringify(value);
        return CryptoJS.SHA1(str).toString();
      },
      sha1: function (value) {
        return CryptoJS.SHA1(value).toString();
      }
    };
  });
