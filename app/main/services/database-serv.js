'use strict';
angular.module('main')
  .factory('$sqldb', function ($log, $q ) {

    var service = this;

    service.checkDB = function () {
      return $q(function (resolve, reject) {
        service.dataBase.transaction(function(tx) {
          tx.executeSql("CREATE TABLE IF NOT EXISTS configuracion (id INT UNIQUE, nombre, tiempo INT, fecha, usuario INT)");
          tx.executeSql("CREATE TABLE IF NOT EXISTS tecnicos (id UNIQUE, nombre, identificacion, clave, email, estado)");
          tx.executeSql("CREATE TABLE IF NOT EXISTS obras (id UNIQUE, nombre, descripcion, tecnico, info )");
          tx.executeSql("CREATE TABLE IF NOT EXISTS servicios (id INT UNIQUE, nombre, formulario, obra)");
        }, function(error) {
          reject(error.message)
        }, function() {
          resolve('ok')
        });
      })
    };

    service.dataBase = "";

    return service;
  });
