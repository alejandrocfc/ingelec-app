'use strict';
angular.module('main')
  .service('$obras', function ($q,$http,$sqldb,$network,crypt,Config) {
    var $obras = {};

    $obras.byUser = function (id) {
      return $q(function (resolve, reject) {
        if($network.checkConnection()){
          var data = {
            contratista:155,
            key: crypt.sha1("*"+Config.key+""+Config.date+"*"),
            login: crypt.sha1(crypt.sha1(Config.login))
          };
          $http.post(Config.baseUrl+"obrasContratistas", data).then(
            function(response) {
              if(response.data.status === 1) {
                var list = response.data.data;
                var obras = [];
                if(list.length>0){
                  $sqldb.dataBase.transaction(function(tx) {
                      list.forEach(function (item) {
                        obras.push({id: item.id_solicitud, nombre: item.nombre_solicitud, descripcion: item.descripcion_solicitud, tecnico: item.contratista_persona_uno, info: JSON.stringify(item)});
                        tx.executeSql("INSERT OR REPLACE INTO obras (id, nombre, descripcion, tecnico, info) VALUES (?,?,?,?,?)",
                          [item.id_solicitud, item.nombre_solicitud, item.descripcion_solicitud, item.contratista_persona_uno, JSON.stringify(item)]);
                      })
                    },
                    function(error) {
                      reject(error.message)
                    },
                    function(){
                      $obras.list = obras;
                      resolve();
                    });
                }else {
                  resolve();
                }
              }
              else reject(response.data.msj);
            },
            function(error) {
              reject(error);
            });
        }else{
          $sqldb.dataBase.transaction(function(tx) {
            tx.executeSql('SELECT * FROM obras', [], function (tx, snap) {
              var obras = [];
              for(var i=0; i < snap.rows.length; i++){
                obras.push(snap.rows.item(i))
              }
              $obras.list = obras;
              resolve()
            }, function (err) {
              reject(err)
            });
          })
        }
      })
    };

    $obras.fetchForm = function (id) {
      return $q(function (resolve, reject) {
        if($network.checkConnection()){
          $http.get(Config.baseUrl+"getFormulario?obra="+id).then(
            function(response) {
              var data = response.data.data;
              if(data.id_formulario) {
                var map  = data.map_formulario;
                var info = {};
                $sqldb.dataBase.transaction(function(tx) {
                  info.id = data.id_formulario;
                  info.nombre = map.name;
                  info.formulario = JSON.stringify(map.fields);
                  info.obra = data.id_solicitud;
                  tx.executeSql("INSERT OR REPLACE INTO servicios (id, nombre, formulario, obra) VALUES (?,?,?,?)", [data.id_formulario, map.name, JSON.stringify(map.fields), data.id_solicitud]);
                },
                function(error) {reject(error.message)},
                function(){resolve(info);});
              }
              else reject(response.data.msj);
            },
            function(error) {
              reject(error);
            });
        }else{
          $sqldb.dataBase.transaction(function(tx) {
            tx.executeSql("SELECT * FROM servicios WHERE obra = ?", [id], function (tx, results) {
              if( results.rows.length > 0 ) {
                resolve(results.rows.item(0))
              }else{
                reject("Usuario no existe. Por favor verifique su conexión")
              }
            }, function (error) {
              reject(error.message)
            });
          })
        }
      })
    }

    return $obras;
  });
