'use strict';
angular.module('servicio', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider) {

  $stateProvider
    .state('main.servicio', {
      url: '/servicio',
      params: {id: null},
      views: {
        homeView: {
          templateUrl: 'servicio/templates/form.html',
          controller: 'ServiceCtrl as ctrl'
        }
      }
    });
});
