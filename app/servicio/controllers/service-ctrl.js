'use strict';
angular.module('servicio')
.controller('ServiceCtrl', function ($obras, $scope, $state, $stateParams, $ionicModal, $ionicActionSheet, $log, $timeout, $camera) {

  $scope.loading = true;
  $scope.steps = [];
  $scope.answers = {};
  $scope.dimension = [];

  //Traer el formulario del servicio
  $obras.fetchForm($stateParams.id).then(function (info) {
    var pos = 0;
    $scope.index = 0;
    $scope.steps = [];
    $scope.steps[pos] = [];
    var fields = JSON.parse(info.formulario);
    fields.forEach(function (field) {
      if(field.type !== "separator") $scope.steps[pos].push(field);
      else{
        pos += 1;
        $scope.steps[pos] = [];
      }
    });
    $scope.loading = false;
  }).catch(function (e) {
    $state.go("main.home");
  });

  $scope.goPrev = function() {
    $scope.index -= 1;
  };

  $scope.goNext = function() {
    $scope.index += 1;
  };

  $scope.save = function() {
    $log.log($scope.answers)
  };

  $scope.image = {};
  $scope.currentSize = 1;
  $scope.sk = false;

  $scope.changeImage = function (library) {
    $camera.get(library).then(function (data) {

      $scope.image.base = data.b64Data;
      $scope.modal.show();
      getImage(data.b64Data)
      //$scope.sk.loadBgImage(this.src, this.width, this.height);
      //$scope.loadSVG(angular.element(document.getElementById('si-canvas')).html(),576,1024, true);
      /*$scope.image.change = true;
      $scope.image.url = URL.createObjectURL(data.blop);*/
    });
  };

  //EDITOR IMAGEN
  $ionicModal.fromTemplateUrl('servicio/templates/modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  // Triggered on a button click, or some other target
  $scope.show = function() {

    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: 'Linea' },
        { text: 'Poli Linea' },
        { text: 'Dibujo Libre' },
        { text: 'Flecha' },
        { text: 'Rectangulo' },
        { text: 'Elipse' }
      ],
      titleText: 'Seleccione el tipo de figura',
      cancelText: 'Cancel',
      cancel: function() {
        // add cancel code..
        hideSheet();
      },
      buttonClicked: function(index) {
        switch (index) {
          case 0:
            $scope.sk.setTool(SkLine);
            break;
          case 1:
            $scope.sk.setTool(SkPolyLine);
            break;
          case 2:
            $scope.sk.setTool(SkFreeDrawing);
            break;
          case 3:
            $scope.sk.setTool(SkArrow);
            break;
          case 4:
            $scope.sk.setTool(SkRect);
            break;
          case 5:
            $scope.sk.setTool(SkEllipse);
            break;
        }
        hideSheet();
        return true;
      }
    });

  };
  $scope.pickColor = function() {

    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: ''},
        { text: ''},
        { text: ''},
        { text: ''},
        { text: ''},
        { text: ''},
        { text: ''}
      ],
      titleText: 'Seleccione el color',
      cssClass:'color-sheet',
      cancelText: 'Cancel',
      cancel: function() {
        // add cancel code..
        hideSheet();
      },
      buttonClicked: function(index) {
        var colores = ["#e00000","#306f1a","#2795ee","#dc7cac","#e1da00","#2f363f","#eaf0f1"];
        $scope.sk.setColor(colores[index]);
        $scope.colorPicked = colores[index];
        hideSheet();
        return true;
      }
    });

  };
  $scope.showSize = function() {
    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: 1 },
        { text: 2 },
        { text: 3 },
        { text: 4 }
      ],
      titleText: 'Seleccione el tamao',
      cancelText: 'Cancel',
      cancel: function() {
        // add cancel code..
        hideSheet();
      },
      buttonClicked: function(index) {
        $scope.currentSize = index+1;
        $scope.sk.setSizeFactor(index+1);
        hideSheet();
        return true;
      }
    });

  };
  function loadSVG(svg,w,h, svgSize)
  {
    var o = $(svg);

    $('#si-canvas').html(o);
    var cw = $('#si-canvas').width();
    var ch = $('#si-canvas').height();
    var viewBox = o[0].viewBox ? o[0].viewBox.baseVal : null;
    var svg_w = viewBox ? viewBox.width : o.width();
    var svg_h = viewBox ? viewBox.height : o.height();

    //console.log("svg_w:",svg_w,"svg_h:", svg_h);
    if(svgSize){
      resizeSVG(svg_w,svg_h);
    }else{
      resizeSVG(w,h);
    }


    $scope.sk = new SketchIn('#svg', svg_w, svg_h, true, cw, ch);
    $scope.sk.sizeOptions = 4;
    // Important for image rendering in canvas
    $scope.sk.s.attr('xmlns:xlink','http://www.w3.org/1999/xlink');
  }

  var gW, gH;
  function resizeSVG(w,h)
  {
    gW = w;
    gH = h;

    var sW = w;
    var sH = h;

    var cW = parseFloat($('#si-canvas').width());
    var cH = parseFloat($('#si-canvas').height());

    if (sW > cW || sH > cH)
    {
      if (sW/sH > cW/cH)
      {
        sH = cW / (sW / sH);
        sW = cW;
      }
      else
      {
        sW = cH * (sW / sH);
        sH = cH;
      }
    }

    var svg = $('#svg').get(0);
    svg.setAttribute('width', sW);
    svg.setAttribute('height', sH);
  }


  function getImage(data){
    var i = new Image();
    i.onload = function() {
      if (!$scope.sk)
        loadSVG('<svg id="svg"></svg>', this.width, this.height);
      else
        resizeSVG(this.width, this.height);

      $scope.sk.loadBgImage(this.src, this.width, this.height);
    };
    i.src = data;
  }

  $scope.rotateSVG = function(){
    if (Object.keys($scope.sk.objects).length && !confirm('This action will remove all drawn objects. Do you want to continue?'))
      return false;
    $scope.sk.rotate();
  };

  $scope.setText = function() {
    $scope.sk.setTool(SkText);
  }

  $scope.zooming = function (flag) {
    if(flag){
      $scope.sk.setTool('zoomIn');
      $scope.sk.zoomIn();
    }else{
      $scope.sk.setTool('zoomOut');
      $scope.sk.zoomOut();
    }
  }

  var tpls = {
      title: "servicio/templates/fields/title.html",
      paragraph: "servicio/templates/fields/paragraph.html",
      input: "servicio/templates/fields/input.html",
      date: "servicio/templates/fields/input.html",
      number: "servicio/templates/fields/number.html",
      textarea: "servicio/templates/fields/text-area.html",
      list: "servicio/templates/fields/list.html",
      select: "servicio/templates/fields/select.html",
      geo: "servicio/templates/fields/geo.html",
      dimension: "servicio/templates/fields/dimension.html",
      image: "servicio/templates/fields/image.html",
  };

  $scope.renderContent = function(type){
    return tpls[type]
  };

  $scope.validForm = function() {
    var form = $scope.steps[$scope.index];
    var required = [];
    form.forEach(function (field) {
      if(field.required) required.push({key:field._id,type:field.type,subtype:field.subtype})
    })
  }

});
