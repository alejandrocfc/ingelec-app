'use strict';
angular.module('register')
.controller('RegisterCtrl', function ($log,$scope,$user,$obras,$state,$ionicHistory,$message,$ionicLoading) {

  $scope.email = "diegopaz1988@gmail.com";
  $scope.registrar = function (email, password) {
    if (!email) {
      $message.alert("Debe ingresar un correo electrónico válido");
    } else if (!password) {
      $message.alert("Debe ingresar una contraseña");
    } else if (password.split("").length < 6) {
      $message.alert("La contraseña debe tener mínimo seis caracteres");
    } else {
      $ionicLoading.show();
      $user.login(email, password).then(function (idUser) {
        $obras.byUser(idUser).then(function () {
          $ionicHistory.nextViewOptions({disableBack: true});
          $state.go("main.home").then(function () {
            $ionicLoading.hide();
          });
        }).catch(function (error) {
          $ionicLoading.hide();
          $message.alert(error);
        });
      }).catch(function (error) {
        $ionicLoading.hide();
        $message.alert(error);
      });
    }
  }

});
