'use strict';
angular.module('register', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider) {

  // ROUTING with ui.router
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('register', {
      url: '/register',
      templateUrl: 'register/templates/register.html',
      controller: 'RegisterCtrl'
    });
});
