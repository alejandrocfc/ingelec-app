'use strict';
angular.module('home', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider) {

  // ROUTING with ui.router
  $stateProvider
      .state('main.home', {
        url: '/home',
        views: {
          'homeView': {
            templateUrl: 'home/templates/home.html',
            controller: 'HomeCtrl as ctrl'
          }
        }
      })
});
