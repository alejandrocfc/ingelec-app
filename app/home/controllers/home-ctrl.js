'use strict';
angular.module('home')
.controller('HomeCtrl', function ($user,$obras,$scope,$state) {

  $scope.obras = $obras.list;

  $scope.link = function (detail) {
    if (!detail) return;
    $state.go('main.details', {detail:detail});
  };

  $scope.service = function (id) {
    $state.go('main.servicio', {id: id});
  }

});
