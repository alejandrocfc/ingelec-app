'use strict';

describe('module: servicio, controller: ServiceCrlCtrl', function () {

  // load the controller's module
  beforeEach(module('servicio'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ServiceCrlCtrl;
  beforeEach(inject(function ($controller) {
    ServiceCrlCtrl = $controller('ServiceCrlCtrl');
  }));

  it('should do something', function () {
    expect(!!ServiceCrlCtrl).toBe(true);
  });

});
